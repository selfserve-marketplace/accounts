# Accounts [![pipeline status](https://gitlab.com/selfserve-marketplace/accounts/badges/master/pipeline.svg)](https://gitlab.com/selfserve-marketplace/accounts/commits/master)

Next generation OPN accounts management service

## Code Coverage

View last code coverage report [here](https://selfserve-marketplace.gitlab.io/accounts).
