const path = require('path');

var config = {}

config.port = process.env.PORT || 3000;
config.baseUrl = '/';
config.jwtSecret = process.env.JWT_SECRET;

config.logPath = process.env.LOG_PATH || path.join(__dirname, '../log');

module.exports = config;
