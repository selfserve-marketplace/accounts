const prodConfig = require('./prod');

const config = Object.assign({}, prodConfig)

module.exports = config;
