const Account = require('../models/account');
const uuid = require('uuid/v4');
const sha512 = require('js-sha512');
const logger = require('../helpers/logger');

const EMAIL_ADDRESS_PATTERN = /^\S+@(\S+)\.(\S+)$/

class AccountsService {
  async createAccount(username, password, firstName, lastName, emailAddress) {
    if (!username) {
      logger.debug('Unable to create account: empty username');
      throw new Error('invalid username');
    }

    if (await this._isUsernameAlreadyInUse(username)) {
      logger.debug('Unable to create account: username already in use');
      throw new Error('username already in use');
    }

    if (!password) {
      logger.debug('Unable to create account: empty password');
      throw new Error('invalid password');
    }

    if (!firstName) {
      logger.debug('Unable to create account: empty firstName');
      throw new Error('invalid firstName');
    }

    if (!lastName) {
      logger.debug('Unable to create account: empty lastName');
      throw new Error('invalid lastName');
    }

    if (!emailAddress || !emailAddress.match(EMAIL_ADDRESS_PATTERN)) {
      logger.debug('Unable to create account: empty or invalid emailAddress format');
      throw new Error('invalid lastName');
    }

    var accountId = uuid();
    var account = {
      accountId: accountId,
      username: username,
      hashedPassword: sha512(password),
      firstName: firstName,
      lastName: lastName,
      emailAddress: emailAddress
    }

    try {
      await Account.create(account);
      logger.info(`Successfully created account with username ${username}`);

      return accountId;
    } catch (err) {
      throw new Error(`Unable to create new account: ${err}`);
    }
  }

  async _isUsernameAlreadyInUse(username) {
    var documents = await Account.find({ username: username });
    return documents.length != 0;
  }

  async findById(accountId) {
    try {
      var documents = await Account.find({ accountId: accountId });
    } catch (err) {
      throw new Error(`Unable to retrieve account information: ${err}`);
    }

    if (documents.length === 0) {
      logger.debug(`Unable to retrieve account information: cannot find account with ID ${accountId}`);
      return undefined;
    }

    var account = documents[0];
    logger.info(`Successfully retrieved account with ID ${accountId}`);

    return {
      username: account.username,
      firstName: account.firstName,
      lastName: account.lastName,
      emailAddress: account.emailAddress
    };
  }

  async authenticate(username, password) {
    var hashedPassword = sha512(password);

    var documents = await Account.find({ username: username });

    if (documents.length === 0) {
      logger.debug(`Unable to authenticate: No account with username [${username}]`);
      return undefined;
    }

    var account = documents[0];

    if (account.username === username && account.hashedPassword === hashedPassword) {
      logger.info(`Authentication successful for user [${username}]. Granting role [${account.role}].`);
      return account.role;
    }

    logger.debug(`Unable to authenticate: Invalid password for [${username}]`);
    return undefined;
  }
}

module.exports = AccountsService;
