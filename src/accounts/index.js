const express = require('express');
const jwt = require('jsonwebtoken');
const logger = require('../helpers/logger');

module.exports = (accountsService, jwtSecret) => {
  const router = express.Router();

  router.post('/', async (req, res) => {
    var username = req.body['username'];
    var password = req.body['password'];
    var firstName = req.body['first_name'];
    var lastName = req.body['last_name'];
    var emailAddress = req.body['email_address'];

    if (!username || !password || !firstName || !lastName || !emailAddress) {
      logger.debug('Invalid account creation request: missing parameters');
      res.status(400).send({ error: 'missing parameters' });
      return;
    }

    try {
      var accountId = await accountsService.createAccount(username, password, firstName, lastName, emailAddress);
      res.location(`/api/accounts/${accountId}`).status(201).send();
    } catch (err) {
      res.status(500).send({ error: err.message });
    }
  });

  router.get('/:id', async (req, res) => {
    var accountId = req.params.id;

    try {
      var accountInformation = await accountsService.findById(accountId);

      if (!accountInformation) {
        res.status(404).send({ error: 'no account matching requested ID' });
        return;
      }

      res.status(200).send({
        username: accountInformation.username,
        first_name: accountInformation.firstName,
        last_name: accountInformation.lastName,
        email_address: accountInformation.emailAddress
      });
    } catch (err) {
      res.status(500).send({ error: err.message });
    }
  });

  router.post('/login', async (req, res) => {
    var username = req.body['username'];
    var password = req.body['password'];

    if (!username || !password) {
      res.status(400).send({ error: 'missing parameters' });
      return;
    }

    try {
      var accountRole = await accountsService.authenticate(username, password);

      if (accountRole === undefined) {
        res.status(401).send({ error: 'invalid username or password' });
        return;
      }

      var authToken = jwt.sign({ role: accountRole }, jwtSecret, { expiresIn: '24h' });
      res.status(200).send({ token: authToken });
    } catch (err) {
      res.status(500).send({ error: err.message });
    }

  });

  return router;
}
