const request = require('supertest');
const accounts = require('./');
const AccountsService = require('./accountsService');
jest.mock('./accountsService');

function createApp(accountsService) {
  const app = require('express')();
  const bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use('/api/accounts', accounts(accountsService, 'mytestingsecret'));

  return app;
}

const ACCOUNT_ID = '1';
const USERNAME = 'username';
const PASSWORD = 'password';
const FIRST_NAME = 'me';
const LAST_NAME = 'myself';
const EMAIL_ADDRESS = 'me@myself.me';

const VALID_ACCOUNT_INFORMATION = {
  username: USERNAME,
  password: PASSWORD,
  first_name: FIRST_NAME,
  last_name: LAST_NAME,
  email_address: EMAIL_ADDRESS
};

describe('POST /api/accounts', () => {
  test('given valid account information then return status created', (done) => {
    AccountsService.mockImplementation(() => ({
      createAccount: jest.fn(() => ACCOUNT_ID)
    }));

    var accountsService = new AccountsService();
    request(createApp(accountsService))
      .post('/api/accounts')
      .send(VALID_ACCOUNT_INFORMATION)
      .expect(201)
      .then(response => {
        expect(accountsService.createAccount).toHaveBeenCalledTimes(1);
        expect(accountsService.createAccount).toHaveBeenLastCalledWith(USERNAME, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
        expect(response.header['location']).toBe(`/api/accounts/${ACCOUNT_ID}`)
        done();
      });
  });

  test('given account creation fails then return status internal server error', (done) => {
    AccountsService.mockImplementation(() => ({
      createAccount: jest.fn(() => { throw new Error('error') })
    }));

    var accountsService = new AccountsService();
    request(createApp(accountsService))
      .post('/api/accounts')
      .send(VALID_ACCOUNT_INFORMATION)
      .expect(500)
      .then(response => {
        expect(accountsService.createAccount).toHaveBeenCalledTimes(1);
        expect(accountsService.createAccount).toHaveBeenLastCalledWith(USERNAME, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS);
        expect(response.body.error).toBe('error');
        done();
      });
  });

  test('given missing parameters then return status bad request', (done) => {
    Object.keys(VALID_ACCOUNT_INFORMATION).forEach(key => {
      var invalidRequestBody = JSON.parse(JSON.stringify(VALID_ACCOUNT_INFORMATION));
      delete invalidRequestBody[key];

      request(createApp(new AccountsService()))
        .post('/api/accounts')
        .send(invalidRequestBody)
        .expect(400)
        .then(response => {
          expect(response.body.error).toBe('missing parameters');
          done();
        });
    });
  });
});

const ACOCUNT_INFORMATION = {
  username: USERNAME,
  firstName: FIRST_NAME,
  lastName: LAST_NAME,
  emailAddress: EMAIL_ADDRESS
};

describe('GET /api/accounts/:id', () => {
  test('given account with requested ID then return status ok', (done) => {
    AccountsService.mockImplementation(() => ({
      findById: jest.fn(() => ACOCUNT_INFORMATION)
    }));

    var accountsService = new AccountsService();
    request(createApp(accountsService))
      .get(`/api/accounts/${ACCOUNT_ID}`)
      .expect(200)
      .then(response => {
        expect(accountsService.findById).toHaveBeenCalledTimes(1);
        expect(accountsService.findById).toHaveBeenLastCalledWith(ACCOUNT_ID);

        expect(response.body['username']).toEqual(USERNAME);
        expect(response.body['first_name']).toEqual(FIRST_NAME);
        expect(response.body['last_name']).toEqual(LAST_NAME);
        expect(response.body['email_address']).toEqual(EMAIL_ADDRESS);
        done();
      });
  });

  test('given no account with requested ID then return status not found', (done) => {
    AccountsService.mockImplementation(() => ({
      findById: jest.fn(() => undefined)
    }));

    var accountsService = new AccountsService();
    request(createApp(accountsService))
      .get(`/api/accounts/${ACCOUNT_ID}`)
      .expect(404)
      .then(response => {
        expect(accountsService.findById).toHaveBeenCalledTimes(1);
        expect(accountsService.findById).toHaveBeenLastCalledWith(ACCOUNT_ID);

        expect(response.body.error).toEqual('no account matching requested ID');
        done();
      });
  });

  test('given account information retrieval fails then return status internal server error', (done) => {
    AccountsService.mockImplementation(() => ({
      findById: jest.fn(() => { throw new Error('error') })
    }));

    var accountsService = new AccountsService();
    request(createApp(accountsService))
      .get(`/api/accounts/${ACCOUNT_ID}`)
      .expect(500)
      .then(response => {
        expect(accountsService.findById).toHaveBeenCalledTimes(1);
        expect(accountsService.findById).toHaveBeenLastCalledWith(ACCOUNT_ID);

        expect(response.body.error).toEqual('error');
        done();
      });
  });
});

const ACCOUNT_ROLE = 'USER';
const VALID_LOGIN_REQUEST = {
  username: USERNAME,
  password: PASSWORD
};

describe('POST /api/accounts/login', () => {
  test('given valid credentials then return status ok', (done) => {
    AccountsService.mockImplementation(() => ({
      authenticate: jest.fn(() => ACCOUNT_ROLE)
    }));

    var accountsService = new AccountsService();
    request(createApp(accountsService))
      .post('/api/accounts/login')
      .send(VALID_LOGIN_REQUEST)
      .expect(200)
      .then(response => {
        expect(accountsService.authenticate).toHaveBeenCalledTimes(1);
        expect(accountsService.authenticate).toHaveBeenLastCalledWith(USERNAME, PASSWORD);

        expect(response.body.token).toEqual(expect.any(String));
        done();
      });
  });

  test('given invalid username or password then return status unauthorized', (done) => {
    AccountsService.mockImplementation(() => ({
      authenticate: jest.fn(() => undefined)
    }));

    var accountsService = new AccountsService();
    request(createApp(accountsService))
      .post('/api/accounts/login')
      .send(VALID_LOGIN_REQUEST)
      .expect(401)
      .then(response => {
        expect(accountsService.authenticate).toHaveBeenCalledTimes(1);
        expect(accountsService.authenticate).toHaveBeenLastCalledWith(USERNAME, PASSWORD);

        expect(response.body.error).toBe('invalid username or password');
        done();
      });
  });

  test('given error retrieving account information then return status internal server error', (done) => {
    AccountsService.mockImplementation(() => ({
      authenticate: jest.fn(() => { throw new Error('error') })
    }));

    var accountsService = new AccountsService();
    request(createApp(accountsService))
      .post('/api/accounts/login')
      .send(VALID_LOGIN_REQUEST)
      .expect(500)
      .then(response => {
        expect(accountsService.authenticate).toHaveBeenCalledTimes(1);
        expect(accountsService.authenticate).toHaveBeenLastCalledWith(USERNAME, PASSWORD);

        expect(response.body.error).toBe('error');
        done();
      });
  });

  test('given missing parameters then return status bad request', (done) => {
    Object.keys(VALID_LOGIN_REQUEST).forEach(key => {
      var invalidRequestBody = JSON.parse(JSON.stringify(VALID_LOGIN_REQUEST));
      delete invalidRequestBody[key];

      request(createApp(new AccountsService()))
        .post('/api/accounts/login')
        .send(invalidRequestBody)
        .expect(400)
        .then(response => {
          expect(response.body.error).toBe('missing parameters');
          done();
        });
    });
  });
});
