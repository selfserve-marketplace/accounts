const mongoose = require('mongoose');
const config = require('../../config');
const logger = require('./logger');

mongoose.connect(config.mongoDbUrl, { useNewUrlParser: true, autoReconnect: true });

mongoose.connection.on('connected', () => {
  logger.info('Mongoose default connection open to %s', config.mongoDbUrl);
});

mongoose.connection.on('error', (err) => {
  logger.error('Mongoose default connection error: %s', err);
});

mongoose.connection.on('disconnected', () => {
  logger.info('Mongoose default connection disconnected');
});

process.on('SIGINT', () => {
  mongoose.connection.close(function () {
    logger.info('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});
